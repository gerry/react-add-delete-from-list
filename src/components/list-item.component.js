import React from 'react'

const ListItem = ({ name, removeItem}) => {
  return (
    <div className='list-item' onClick={() => removeItem(name)}>{name}</div>
  )
}

export default ListItem;