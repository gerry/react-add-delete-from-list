import React, { useState } from 'react'
import ListItem from './list-item.component';

const List = () => {
  const [list, setList] = useState([]);
  const [itemInput, setItemInput] = useState("");

  const addItemToList = () => {
    if(itemInput) {
      setList([...list, itemInput]);
      setItemInput("");
    }
  }

  const removeItemFromList = (item) => {
    const filteredList = list.filter(listItem => listItem !== item);
    setList(filteredList);
  }

  const handleEnterKey = (event) => {
    if(event.keyCode === 13){
      addItemToList();
    }
  }

  return (
    <div className='list'>
      <input type={`text`} value={itemInput} onKeyDown={handleEnterKey} onChange={(e) => setItemInput(e.target.value)} placeholder="Enter an item name" name="item-input" />
      <button type='button' onClick={addItemToList}>Add</button>
      {list.length > 0 && <hr />}
      {
        list.map((item, index) => (
          <ListItem name={item} removeItem={removeItemFromList} />
        ))
      }
    </div>
  )
}

export default List