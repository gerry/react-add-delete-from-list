import React from 'react';
import List from './components/list.component';

import './App.css';

function App() {
  return (
    <div className="App">
      <h1>Add / Remove from List</h1>
      <List />
    </div>
  );
}

export default App;
